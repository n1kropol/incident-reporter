import {fixture, test} from "testcafe";

const authenticationAPI = require('./api/authentication');
const reportAPI = require('./api/report');

fixture('Incident reporter');

test(`Sending successful report`, async () => {
    const PRIORITY = 1;
    const REPORT = 'report';
    const TOKEN = await authenticationAPI.doLogin();
    await reportAPI.submit(TOKEN, PRIORITY, REPORT);
});

test(`Sending report with invalid data`, async () => {
    const PRIORITY = 6;
    const REPORT = 1;
    const TOKEN = await authenticationAPI.doLogin();
    await reportAPI.submit(TOKEN, PRIORITY, REPORT);
});