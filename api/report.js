import rp from "request-promise-native";

const BASE_URL = process.env.domain;
const API = process.env.api;
const SUBMIT_REPORT = process.env.submit_report;

async function submit(token, priority, report) {
    let options = {
        method: 'POST',
        uri: BASE_URL + API + SUBMIT_REPORT,
        headers: {
            'Content-Type' : 'application/json',
            'Authorization': token
        },
        body: {
            'priority': priority,
            'report'  : report
        }
    };
    return rp(options)
        .then(function (parsedBody) {
            assert(parsedBody).to.have.status(200);
            console.log(`Report successfully submitted`);
        })
        .catch(function (err) {
            console.error("Sending report failed: ", err);
        });
}

module.exports.submit = submit();