const rp = require('request-promise-native');

const BASE_URL = process.env.domain;
const API = process.env.api;
const AUTH = process.env.auth;
const USERNAME = process.env.username;
const PASSWORD = process.env.password;

async function doLogin() {
    let options = {
        method: 'POST',
        uri: BASE_URL + API + AUTH,
        headers: {
            'Content-Type': 'application/json'
        },
        body: {
            'username': USERNAME,
            'password': PASSWORD
        }
    };
    return rp(options)
        .then(function (parsedBody) {
            const TOKEN = parsedBody.token;
            assert(parsedBody).to.have.status(200);
            expect(parsedBody).to.exist('token');
            console.log(`Login successfully passed`);
            return TOKEN;
        })
        .catch(function (err) {
            console.error("Login failed: ", err);
        });
}

module.exports.doLogin = doLogin();