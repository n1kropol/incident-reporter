# Incident reporter
Incident reporter service API testing

# Before start

Run following command to install it:
`sudo npm install testcafe`
`sudo npm install request-promise-native`
`sudo npm install chai`
`sudo npm install chai-http`

# Setup environment
## MacOS
export BASE_URL="http://mysite.com"
export API="/api"
export AUTH="/auth"
export SUBMIT_REPORT="/submit_report/"
## For Windows use e.g.
set BASE_URL="http://mysite.com"
and so on

# Run tests
testcafe chrome:headless ./tests.js
